fastHTTP
======



A threaded python http client

   


Install
-------------------------
    pip install requiremets/install.txt
    pip install -e .[test]


Then give the command line program a try.

.. code-block:: console

    fastHTTP 100 urls/random_urls.txt



Documentation
--------

Comments in fastHTTP/clients.py


Notes
-----------
Attempted most barebones version of threading and also using the requests library.
Requests library does appear to be the fastest using a small sample of urls.
I knock out my internet connection when I try the urls/random_urls.txt file

