__author__ = 'jcrubino'


import logging
from time import time, sleep
from threading import Thread
from urllib.request import urlopen

# for python 2/3 compatability

from queue import Queue


# third party lib that does connection pooling (reuse of socket for urls to same host)
import requests



class ThreadedRequestsClient(object):
    """
    Async HTTP GET Client for Requesting Data From Exchanges
    Not for POST requests!
    **Uses requests library and python threading**
    parameters:
        job_list == url list
        a tuple of tuples (exchange, trade_pair, url)

    returns:
        None
        Sends one round of calls with AsyncExchangeClient.run()
        Threads loads data into redis
    """
    def __init__(self, job_list, threads=10, client='req'):
        self.job_list = job_list
        self.Thread = Thread
        self.num_threads = threads
        self.time = time
        self.q = Queue(maxsize=int(len(job_list) * 1.1))
        self.out_q = Queue()
        self.run_time = 0
        self.sleep = sleep
        self.client = client
        if client == 'req':
            self.client =  requests 
        else: 
            self.client = urlopen
        self.__cl__ = client



    def do_work(self):

        while True:
            try:
                url= self.q.get()
                if url == None:
                    return
             
                r =list( self.get_response(url))
                self.out_q.put(r)
                self.q.task_done()
            except Exception as e:
                self.q.task_done()
                self.out_q.put((str(e), url))


    def get_response(self, url):
        '''
        :param data:
            Format:
                 url
            Example:
                http://www.google.com'
        :return: status, attempts, url
        '''
        attempt = 0
        while attempt < 3:
            try:
                if self.__cl__ == 'req':
                    res = requests.get(url)
                    if res.status_code == 200:
                        return res.status_code, url, attempt
                else:
                    res = urlopen(url)
                    if res.code == 200:
                        return res.code , url, attempt        
                attempt += 1
            except Exception as e:
                return (e,attempt, url)
        if attempt >= 3:
            if self.__cl__ == 'res':
                code = res.status_code
            else:
                code = res.code             
            return (code, url, attempt)

    def run(self):
        # dameonized threaded http requests
        try:

            for i in range(self.num_threads):
                worker = self.Thread(target=self.do_work)
                worker.daemon = True
                worker.start()
            start_time = self.time()
            for url in self.job_list:
                self.q.put(url)

            self.q.join()
            for i in range(self.num_threads):
                self.q.put(None)
            
            
        except KeyboardInterrupt:
            for i in range(self.num_threads):
                self.q.put(None)
        except Exception as exc:
            print(exc)

    def results(self):
        self.results = [x for x in self.out_q.queue]
        self.good = [x for x in self.results if x[0] == 200]
        self.bad = [ x for x in self.results if x[0] != 200]
        return {'good':self.good, 'bad':self.bad}



# class ThreadedHTTPClient(object):
#     # this implementation uses only the standard library
#     def __init__(self, url_list, threads=10, interval=0.05):
#         self.url_list = url_list
#         self.start =  None
#         self.end   = None
#         self.num_threads = threads
#         self.in_q  = Queue()
#         self.out_q = Queue()
#         self.interval = interval

#     # this function is a thread safe function that only accesses the shared queue system
#     def work_thread(self):
#         last = None
#         while 1:

#             url = self.in_q.get()
#             if last == url:
#                 continue
#             last = url
#             if url == None:
#                 return None
#             response = None
#             start = time()
#             attempts = 1
#             if type(url) == str:
#                 while attempts <= 4:
#                     request = urlopen(url)
#                     if request.code == 200:
#                         self.out_q.put((200, url, attempts))
#                         break             
#                     attempts += 1
#                     sleep(0.01)
#                 self.out_q.put((request.code, url, 3))





#     def run(self):
#         # dameonized threaded http requests
#         try:
#             # activate threads
#             for i in range(self.num_threads):
#                 worker = Thread(target=self.work_thread)
#                 worker.daemon = True
#                 worker.start()
#                 # puting an interval inbetween threads may help when many threads are spawned
#                 # default is 0.0
#                 sleep(self.interval)
#             start_time = time()
#             # load urls into queue
#             for item in self.url_list:
#                 self.in_q.put(item)
#             # blocks until all urls sent to a thread
#             self.in_q.join()
#             # send kill thread signal
#             for i in range(self.num_threads):
#                 self.in_q.put(None)
#             # calculate elapsed time
#             self.run_time = time() - start_time
#             print("ThreadedHTTPClient Elapsed Runtime: {0}".format(self.run_time))
#             self.results = [x for x in self.out_q.queue]
#             self.good = [x for x in self.results if x[0] == 200]
#             self.bad = [ x for x in self.results if x[0] != 200]


#         except KeyboardInterrupt:
#             for i in range(self.num_threads):
#                 self.in_q.put(None)


#         except Exception as exc:
#             print(exc)


if __name__ == "__main__":


    URLS = ['http://www.foxnews.com/',
    'http://www.cnn.com/',
    'http://europe.wsj.com/',
    'http://www.bbc.co.uk/',
    'https://www.google.com',
    'https://www.reddit.com',
    'https://www.imgur.com',
    'https://www.yahoo.com',
    'https://www.amazon.com',
    'https://www.ebay.com',
    'https://www.bidspotter.com',
    'http://www.digitalnomads.us',
    'https://www.twitter.com',
    'https://www.github.com',
    'https://nomadlist.com'
    ]
    a = time()
    print('ThreadedRequestsClient Test')
    test = ThreadedRequestsClient(URLS)
    test.run()
    b = time()
    print('Elapsed time: {0}'.format(b-a))





