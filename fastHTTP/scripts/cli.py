# Skeleton of a CLI
import time
import click
from fastHTTP.clients import ThreadedRequestsClient 


@click.command('fastHTTP')
@click.argument('threads', type=int, metavar='threads')
@click.argument('filename', type=str, metavar='filename')
def cli(threads, filename):
    """use n threads to process text file of line deliminated urls"""
    with open(filename) as f:
    	urls = f.read().splitlines()
    	urls = [x.strip('\n') for x in urls]
    client = ThreadedRequestsClient(urls, threads)
    start = time.time()
    client.run()
    end   = time.time()

    results = client.results()
    print("Results processed in {0} seconds.".format(end-start))
    print('Results stored in results.csv')
    with open('results.csv', 'w') as f:
    	for item in results['good']:
    		f.write('good,'+','.join(item)+'\n')
    	for item in results['bad']:
    		f.write('bad,'+','.join(item)+'\n')

