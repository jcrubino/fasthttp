from fastHTTP.clients import ThreadedRequestsClient, ThreadedHTTPClient


URLS = ['http://www.foxnews.com/',
    'http://www.cnn.com/',
    'http://europe.wsj.com/',
    'http://www.bbc.co.uk/',
    'https://www.google.com',
    'https://www.reddit.com',
    'https://www.imgur.com',
    'https://www.yahoo.com',
    'https://www.aws.amazon.com',
    'https://www.amazon.com',
    'https://www.ebay.com',
    'https://www.bidspotter.com',
    'http://www.digitalnomads.us',
    'https://www.twitter.com',
    'https://www.github.com',
    'https://nomadlist.com'
    ]






def test_requests_client():
	test = ThreadedHTTPClient(URLS)
	test.run()

def test_bare_client():
	test = ThreadedHTTPClient(URLS)
	test.run()
    
